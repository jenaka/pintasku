@extends('front.index')

@section('content')
<!-- ABOUT UA AREA -->
<section class="about-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="about-content-table">
                    <div class="about-content-table-sell">
                        <div class="about-heading">
                            <h2>About Flake</h2>
                        </div>
                        <div class="about-links">
                            <ul>
                                <li><a href="index.html">Home <i class="fa fa-angle-left"></i></a></li>
                                <li><a href="about.html"> About us</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- ABOUT UA AREA END -->

<!-- WELCOME AREA  -->

<section class="welcome-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="welcome-content">
                    <h2>Welcome to <span>Flake</span> </h2>
                    <p class="welcome-p1">Lorem ipsum duis aaboty autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie conse quat, vel illum dolore eu feugiat nulla facilisis at vero eros etaran aaccumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
                    <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imper diet doming id quod mazim placerat facer possim assum. Soluta nobis eleifend option congue nihil imper</p>
                    <div class="welcome-social-links">
                        <div class="social-links">
                            <a href="#" target="_blank"><span class="ti-facebook"></span></a>
                            <a href="#" target="_blank"><span class="ti-twitter-alt"></span></a>
                            <a href="#" target="_blank"><span class="ti-vimeo-alt"></span></a>
                            <a href="#" target="_blank"><span class="ti-pinterest"></span></a>
                            <a href="#" target="_blank"><span class="ti-google"></span></a>
                        </div>
                    </div>
                </div>
                <div class="welcome-man-pic">
                    <img src="asset/img/slider-img/welcome-man.png" alt="man picture">
                </div>
            </div>
        </div>
    </div>
</section>


<!-- WELCOME AREA END -->

<!--FUN FACT AREA AREA-->

<section class="fan-fact-area fan-fact-area-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 ">
                <div class="single-items">
                    <h2><span class="counter">1350</span>+</h2>
                    <img src="asset/img/fun-fact/fan-fact-1.png" alt="fan-fact-1">
                    <h4>Registared Domains</h4>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 ">
                <div class="single-items">
                    <h2><span class="counter">950</span>+</h2>
                    <img src="asset/img/fun-fact/fan-fact-2.png" alt="fan-fact-2">
                    <h4>Sites Hosted</h4>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 ">
                <div class="single-items">
                    <h2><span class="counter">1080</span>+</h2>
                    <img src="asset/img/fun-fact/fan-fact-3.png" alt="fan-fact-3">
                    <h4>Happy Clients</h4>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="single-items">
                    <h2><span class="counter">226</span>+</h2>
                    <img src="asset/img/fun-fact/fan-fact-4.png" alt="fan-fact-4">
                    <h4>Awards Won</h4>
                </div>
            </div>
        </div>
    </div>
</section>

<!--FUN FACT AREA AREA END-->

<!--SERVICE AREA-->

<section class="service-area wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.4s">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title">
                    <h2>Why <span>Flake</span> is So Good?</h2>
                    <img src="asset/img/section-shape.png" alt="section-shape">
                    <p>Lorem ipsum dolor sittem ametamngcing elit, per sed do eiusmoad teimpor sittemelit inuning ut sed.</p>
                </div>
            </div>
        </div>
        <div class="service-teatimonial">
            <div class="service-teatimonial-single-item ">
                <div class="service-teatimonial-item-inner">
                    <img src="asset/img/service/service-icon.png" alt="service-icon">
                    <h2>Resellers Hosting</h2>
                    <p>Lorem ipsum dolor sittem ametam ngcing elit, per sed do eiusmoad teimpor inunt ut .</p>
                </div>
            </div>
            <div class="service-teatimonial-single-item ">
                <div class="service-teatimonial-item-inner">
                    <img src="asset/img/service/service-icon-2.png" alt="service-icon-2">
                    <h2>Resellers Hosting</h2>
                    <p>Lorem ipsum dolor sittem ametam ngcing elit, per sed do eiusmoad teimpor inunt ut .</p>
                </div>
            </div>
            <div class="service-teatimonial-single-item ">
                <div class="service-teatimonial-item-inner">
                    <img src="asset/img/service/service-icon-3.png" alt="service-icon-3">
                    <h2>Resellers Hosting</h2>
                    <p>Lorem ipsum dolor sittem ametam ngcing elit, per sed do eiusmoad teimpor inunt ut .</p>
                </div>
            </div>
            <div class="service-teatimonial-single-item ">
                <div class="service-teatimonial-item-inner">
                    <img src="asset/img/service/service-icon.png" alt="service-icon-4">
                    <h2>Resellers Hosting</h2>
                    <p>Lorem ipsum dolor sittem ametam ngcing elit, per sed do eiusmoad teimpor inunt ut .</p>
                </div>
            </div>
            <div class="service-teatimonial-single-item ">
                <div class="service-teatimonial-item-inner">
                    <img src="asset/img/service/service-icon-2.png" alt="service-icon-5">
                    <h2>Resellers Hosting</h2>
                    <p>Lorem ipsum dolor sittem ametam ngcing elit, per sed do eiusmoad teimpor inunt ut .</p>
                </div>
            </div>
            <div class="service-teatimonial-single-item ">
                <div class="service-teatimonial-item-inner">
                    <img src="asset/img/service/service-icon-3.png" alt="service-icon-6">
                    <h2>Resellers Hosting</h2>
                    <p>Lorem ipsum dolor sittem ametam ngcing elit, per sed do eiusmoad teimpor inunt ut .</p>
                </div>
            </div>
        </div>
        <div class="slider-btns-1">
            <button class="service-nav-left"><span class="ti-angle-left"></span></button>
            <button class="service-nav-right"><span class="ti-angle-right"></span></button>
        </div>
    </div>
</section>
<!--SERVICE TWO  END-->

<!--TEAM AREA-->

<section class="team-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title">
                    <h2>Why <span>Flake</span> is So Good?</h2>
                    <img src="asset/img/section-shape.png" alt="section shape">
                    <p>Lorem ipsum dolor sittem ametamngcing elit, per sed do eiusmoad teimpor sittemelit inuning ut sed sittem do eiusmod.</p>
                </div>
            </div>
        </div>
        <div class="team-carosuel">
            <div class="team-carosule-single-item">
                <img src="asset/img/team/team-member.jpg" alt="team member">
                <div class="team-meta">
                    <h4>Anna Watson</h4>
                    <p>Strategic Planner</p>
                </div>
                <div class="team-overlay">
                    <h5>Anna Watson</h5>
                    <p class="meta-p">Founder</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna </p>
                    <div class="team-social-links">
                        <div class="social-links">
                            <a href="#" target="_blank"><span class="ti-facebook"></span></a>
                            <a href="#" target="_blank"><span class="ti-twitter-alt"></span></a>
                            <a href="#" target="_blank"><span class="ti-vimeo-alt"></span></a>
                            <a href="#" target="_blank"><span class="ti-pinterest"></span></a>
                            <a href="#" target="_blank"><span class="ti-google"></span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="team-carosule-single-item">
                <img src="asset/img/team/team-member-2.jpg" alt="team member">
                <div class="team-meta">
                    <h4>M S Newaz</h4>
                    <p>Strategic Planner</p>
                </div>
                <div class="team-overlay">
                    <h5>M S Newaz</h5>
                    <p class="meta-p">Strategic Planner</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna </p>
                    <div class="team-social-links">
                        <div class="social-links">
                            <a href="#" target="_blank"><span class="ti-facebook"></span></a>
                            <a href="#" target="_blank"><span class="ti-twitter-alt"></span></a>
                            <a href="#" target="_blank"><span class="ti-vimeo-alt"></span></a>
                            <a href="#" target="_blank"><span class="ti-pinterest"></span></a>
                            <a href="#" target="_blank"><span class="ti-google"></span></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="team-carosule-single-item">
                <img src="asset/img/team/team-member-3.jpg" alt="team member">
                <div class="team-meta">
                    <h4>Cristian Elora</h4>
                    <p>Art Director</p>
                </div>
                <div class="team-overlay">
                    <h5>Cristian Elora</h5>
                    <p class="meta-p">Art Director</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna </p>
                    <div class="team-social-links">
                        <div class="social-links">
                            <a href="#" target="_blank"><span class="ti-facebook"></span></a>
                            <a href="#" target="_blank"><span class="ti-twitter-alt"></span></a>
                            <a href="#" target="_blank"><span class="ti-vimeo-alt"></span></a>
                            <a href="#" target="_blank"><span class="ti-pinterest"></span></a>
                            <a href="#" target="_blank"><span class="ti-google"></span></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="team-carosule-single-item">
                <img src="asset/img/team/team-member-4.jpg" alt="team member">
                <div class="team-meta">
                    <h4>Ohidul Islam</h4>
                    <p>Web Developer</p>
                </div>
                <div class="team-overlay">
                    <h5>Ohidul Islam</h5>
                    <p class="meta-p">Web Developer</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna </p>
                    <div class="team-social-links">
                        <div class="social-links">
                            <a href="#" target="_blank"><span class="ti-facebook"></span></a>
                            <a href="#" target="_blank"><span class="ti-twitter-alt"></span></a>
                            <a href="#" target="_blank"><span class="ti-vimeo-alt"></span></a>
                            <a href="#" target="_blank"><span class="ti-pinterest"></span></a>
                            <a href="#" target="_blank"><span class="ti-google"></span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="team-carosule-single-item">
                <img src="asset/img/team/team-member.jpg" alt="team member">
                <div class="team-meta">
                    <h4>Anna Watson</h4>
                    <p>Strategic Planner</p>
                </div>
                <div class="team-overlay">
                    <h5>Anna Watson</h5>
                    <p class="meta-p">Strategic Planner</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna </p>
                    <div class="team-social-links">
                        <div class="social-links">
                            <a href="#" target="_blank"><span class="ti-facebook"></span></a>
                            <a href="#" target="_blank"><span class="ti-twitter-alt"></span></a>
                            <a href="#" target="_blank"><span class="ti-vimeo-alt"></span></a>
                            <a href="#" target="_blank"><span class="ti-pinterest"></span></a>
                            <a href="#" target="_blank"><span class="ti-google"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--TEAM AREA  END-->

<!--TESTIMONIAL-AREA -->

<section class="testimonial-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title">
                    <h2>Which People Love <span>Flake</span> </h2>
                    <img src="asset/img/section-shape.png" alt="section shape">
                    <p>Lorem ipsum dolor sittem ametamngcing elit, per sed do eiusmoad teimpor sittemelit inuning ut sed sittem do eiusmod.</p>
                </div>
            </div>
        </div>
        <div class="testi-carousel">
            <div class="testi-single-item ">
                <div class="item-inner">
                    <img src="asset/img/testimonial/testimonial-icon-1.png" alt="testimonial-icon">
                    <p>Lorem ipsum dolor sit amet, consectetur adip mayal eiusmod tempor incubtconsectetur aliqua. Ultra enim ad nim veniam, quis nostrud exercitation ullamco</p>
                    <div class="testimonial-meta">
                        <h4>Michael Clarke</h4>
                        <p>marveltheme.com</p>
                    </div>
                </div>
            </div>
            <div class="testi-single-item wow ">
                <div class="item-inner">
                    <img src="asset/img/testimonial/testimonial-icon-1.png" alt="testimonial-icon">
                    <p>Lorem ipsum dolor sit amet, consectetur adip mayal eiusmod tempor incubtconsectetur aliqua. Ultra enim ad nim veniam, quis nostrud exercitation ullamco</p>
                    <div class="testimonial-meta">
                        <h4>Michael Clarke</h4>
                        <p>marveltheme.com</p>
                    </div>
                </div>
            </div>
            <div class="testi-single-item ">
                <div class="item-inner">
                    <img src="asset/img/testimonial/testimonial-icon-1.png" alt="testimonial-icon">
                    <p>Lorem ipsum dolor sit amet, consectetur adip mayal eiusmod tempor incubtconsectetur aliqua. Ultra enim ad nim veniam, quis nostrud exercitation ullamco</p>
                    <div class="testimonial-meta">
                        <h4>Michael Clarke</h4>
                        <p>marveltheme.com</p>
                    </div>
                </div>
            </div>
            <div class="testi-single-item ">
                <div class="item-inner">
                    <img src="asset/img/testimonial/testimonial-icon-1.png" alt="testimonial-icon">
                    <p>Lorem ipsum dolor sit amet, consectetur adip mayal eiusmod tempor incubtconsectetur aliqua. Ultra enim ad nim veniam, quis nostrud exercitation ullamco</p>
                    <div class="testimonial-meta">
                        <h4>Michael Clarke</h4>
                        <p>marveltheme.com</p>
                    </div>
                </div>
            </div>
            <div class="testi-single-item ">
                <div class="item-inner">
                    <img src="asset/img/testimonial/testimonial-icon-1.png" alt="testimonial-icon">
                    <p>Lorem ipsum dolor sit amet, consectetur adip mayal eiusmod tempor incubtconsectetur aliqua. Ultra enim ad nim veniam, quis nostrud exercitation ullamco</p>
                    <div class="testimonial-meta">
                        <h4>Michael Clarke</h4>
                        <p>marveltheme.com</p>
                    </div>
                </div>
            </div>
            <div class="testi-single-item ">
                <div class="item-inner">
                    <img src="asset/img/testimonial/testimonial-icon-1.png" alt="testimonial-icon">
                    <p>Lorem ipsum dolor sit amet, consectetur adip mayal eiusmod tempor incubtconsectetur aliqua. Ultra enim ad nim veniam, quis nostrud exercitation ullamco</p>
                    <div class="testimonial-meta">
                        <h4>Michael Clarke</h4>
                        <p>marveltheme.com</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="slider-btns">
            <button class="testi-nav-left"><span class="ti-angle-left"></span></button>
            <button class="testi-nav-right"><span class="ti-angle-right"></span></button>
        </div>
    </div>
</section>

<!--TESTIMONIAL-AREA  END-->



<!--BRANDE AREA -->

<section class="brand-area ">
    <div class="container">
        <div class="all-brand-carsouel">

            <div class="brand-single-item">
                <img src="asset/img/brand-logo/brand-icon-2.png" alt="brand-icon">
            </div>

            <div class="brand-single-item">
                <img src="asset/img/brand-logo/brand-icon-3.png" alt="brand-icon">
            </div>

            <div class="brand-single-item">
                <img src="asset/img/brand-logo/brand-icon-4.png" alt="brand-icon">
            </div>

            <div class="brand-single-item">
                <img src="asset/img/brand-logo/brand-icon-5.png" alt="brand-icon">
            </div>

            <div class="brand-single-item">
                <img src="asset/img/brand-logo/brand-icon-6.png" alt="brand-icon">
            </div>

            <div class="brand-single-item">
                <img src="asset/img/brand-logo/brand-icon-2.png" alt="brand-icon">
            </div>

            <div class="brand-single-item">
                <img src="asset/img/brand-logo/brand-icon-6.png" alt="brand-icon">
            </div>

            <div class="brand-single-item">
                <img src="asset/img/brand-logo/brand-icon-2.png" alt="brand-icon">
            </div>
        </div>
    </div>
</section>

<!--BRANDE AREA  END-->

<!--CTA AREA -->

<section class="call-to-action-area">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-12 ">
                <div class="call-to-action-area-content">
                    <span>2500+ People trust Flake</span>
                    <h2>Get to start Flake right now ?</h2>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 ">
                <div class="call-to-action-btn">
                    <a class="pricing-btn blue-btn homepage-one-all-features-btn action-btn" href="#">Get Start Now</a>
                </div>
            </div>
        </div>
    </div>
</section>

<!--CTA AREA  END-->
@endsection