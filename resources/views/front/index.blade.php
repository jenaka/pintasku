<!DOCTYPE html>
<html lang="zxx">

<head>
    <!-- BASIC META-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Flake best web hosting templete</title>
    <!-- FAVICON -->
    <link rel="apple-touch-icon" href="{{ asset('assets/img/favicon/apple-touch-icon.png') }}">
    <link rel="icon" href="{{ asset('assets/img/favicon/favicon.ico') }}">
    <!-- WEB FONTS  -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- BOOTSTRAP MIN CSS -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- ALL PLUGINS CSS -->
    <link href="{{ asset('assets/css/elements.css') }}" rel="stylesheet">
    <!-- THEME STYLE CSS -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <!-- RESPONSIVE CSS -->
    <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet">
    <!-- HEAD LIBS -->
    <script src="{{ asset('assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>

</head>

<body>
    <!--[if lt IE 8]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
      <![endif]-->
    <!-- HEADER TOP -->
    @include('front.temp.top_menu')

    <!-- NAVIGATION -->
    @include('front.temp.menu')
    <!-- /MOBILE NAVIGATION -->
    <!-- /NAVIGATION -->


    @yield('content')

    <!--FOOTER AREA -->

    <section class="footer-area">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 ">
                    <div class="footer-single-item">
                        <div class="logo">
                            <a href="index.html"><img src="{{ asset('assets/img/logo/logo.png') }}" alt="logo"></a>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consec tetura adipisicing elit, sed temporia incididunt.</p>
                        <p class="contact-info"><span class="ti-headphone-alt"></span><a href="tel:+8801711991383">+0044 545 989 626</a></p>
                        <p class="contact-info"><span class=" ti-email"></span><a href="mailto:hasan.nazmulmmc@gamil.com">www.yourwebsite.com</a> </p>
                        <p class="contact-info"><span class="ti-location-pin"></span><a href="#">28 Green Tower, Street Name,<br>New York City, USA</a></p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 ">
                    <div class="single-footer margin-left">
                        <h2>About</h2>
                        <div class="footer-links">
                            <ul>
                                <li><a href="index.html">Home Page</a></li>
                                <li><a href="about.html">About Us</a></li>
                                <li><a href="#">WHMCS</a></li>
                                <li><a href="domain.html">Search Domain</a></li>
                                <li><a href="#">My Account</a></li>
                                <li><a href="#">Support Center</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 ">
                    <div class="single-footer">
                        <h2>Services</h2>
                        <div class="footer-links">
                            <ul>
                                <li><a href="hosting.html">Web Hosting</a></li>
                                <li><a href="#">Reseller Hosting</a></li>
                                <li><a href="vps-hosting.html">VPS Hosting</a></li>
                                <li><a href="hosting.html">Web Hosting</a></li>
                                <li><a href="#">Email Mrketing</a></li>
                                <li><a href="vps-hosting.html">VPS Hosting</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 ">
                    <div class="single-footer">
                        <h2>Newsletter</h2>
                        <form action="http://hackinout.us15.list-manage.com/subscribe/post-json?u=e44c1f194bec93e238615469e&amp;id=fa63cb4ac7&c=?" method="get" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate">
                            <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Type our mail address" required>
                            <div style="position: absolute; left: -5000px;" aria-hidden="true">
                                <input type="text" name="b_e44c1f194bec93e238615469e_f6f826e769" tabindex="-1" value="">
                            </div>
                            <input type="submit" value="subscribe" name="subscribe" id="mc-embedded-subscribe" class="mc-button">
                            <div id="subscribe-result">
                            </div>
                        </form>
                        <div class="social-links">
                            <a href="#" target="_blank"><span class="ti-facebook"></span></a>
                            <a href="#" target="_blank"><span class="ti-twitter-alt"></span></a>
                            <a href="#" target="_blank"><span class="ti-vimeo-alt"></span></a>
                            <a href="#" target="_blank"><span class="ti-pinterest"></span></a>
                            <a href="#" target="_blank"><span class="ti-google"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--FOOTER AREA  END-->



    <!--FOOTER-BOTTOM-AREA -->

    <section class="foter-botom-area ">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 ">
                    <p>Copyright &copy; 2017<a href="http://marveltheme.com/">marveltheme</a>all right reserved.</p>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 ">
                    <div class="payment-getway text-right">
                        <img src="{{ asset('assets/img/bg/footer-bottom.png') }}" alt="payment-icon">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--FOOTER-BOTTOM-AREA -->
    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins.js') }}"></script>
    <script src="{{ asset('assets/js/main.js') }}"></script>
    <script src="{{ asset('assets/js/ajax-mail.js') }}"></script>
    <script src="{{ asset('assets/js/ajax-subscribe.js') }}"></script>

</body>

</html>
