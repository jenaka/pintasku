<nav id="sidebar">
        <!-- Sidebar Scroll Container -->
        <div id="sidebar-scroll">
            <!-- Sidebar Content -->
            <div class="sidebar-content">
                <!-- Side Header -->
                <div class="content-header content-header-fullrow px-15" style="background-color: #2d3238;">
                    <!-- Mini Mode -->
                    <div class="content-header-section sidebar-mini-visible-b">
                        <!-- Logo -->
                        <span class="content-header-item font-w700 font-size-xl float-left animated fadeIn">
                            <span class="text-dual-primary-dark">c</span><span class="text-primary">b</span>
                        </span>
                        <!-- END Logo -->
                    </div>
                    <!-- END Mini Mode -->
    
                    <!-- Normal Mode -->
                    <div class="content-header-section text-center align-parent sidebar-mini-hidden">
                        <!-- Close Sidebar, Visible only on mobile screens -->
                        <!-- Layout API, functionality initialized in Codebase() -> uiApiLayout() -->
                        <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout" data-action="sidebar_close">
                            <i class="fa fa-times text-danger"></i>
                        </button>
                        <!-- END Close Sidebar -->
    
                        <!-- Logo -->
                        <div class="content-header-item" >
                            <a class="link-effect font-w700" href="">
                                <i class="si si-fire text-primary"></i>
                                <span class="font-size-xl text-dual-primary-dark"></span><span class="font-size-xl text-primary">PINTASKU</span>
                            </a>
                        </div>
                        <!-- END Logo -->
                    </div>
                    <!-- END Normal Mode -->
                </div>
                <!-- END Side Header -->
    
                <!-- Side User -->
                
                <!-- END Side User -->
    
                <!-- Side Navigation -->
                <div class="content-side content-side-full">
                    <ul class="nav-main">
                        <li>
                            <a href="{{ route('admin.home') }}" class=""><i class="si si-compass"></i><span class="sidebar-mini-hide">Dashboard</span></a>
                        </li>
                        <li>
                            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-users"></i><span class="sidebar-mini-hide">Client</span></a>
                            <ul>
                                <li>
                                    <a href="">Add New Client</a>
                                </li>
                                <li>
                                    <a href="{{ route('admin.clients') }}">List Clients</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-users"></i><span class="sidebar-mini-hide">Products</span></a>
                            <ul>
                                <li>
                                    <a href="">Add New Products</a>
                                </li>
                                <li>
                                    <a href="">List Product</a>
                                </li>
                                <li>
                                    <a href="">Category</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-briefcase"></i><span class="sidebar-mini-hide">Services</span></a>
                            <ul>
                                <li>
                                    <a href="">Domain Names</a>
                                </li>
                                <li>
                                    <a href="">Hosting Packages</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-wallet"></i><span class="sidebar-mini-hide">Financial</span></a>
                            <ul>
                                <li>
                                    <a href="">Pemasukan</a>
                                </li>
                                <li>
                                    <a href="">Pengeluaran</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- END Side Navigation -->
            </div>
            <!-- Sidebar Content -->
        </div>
        <!-- END Sidebar Scroll Container -->
    </nav>