<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('front.index');
// });

// Route::get('/contact', function () {
//     return view('front.index');
// });

Auth::routes();


// ROUTE UNTUK HALAMAN AWAL 
Route::get('/', 'HomeController@index')->name('home');
Route::get('/about-us', 'HomeController@about')->name('about');
Route::get('/products', 'ProductController@index')->name('product');
Route::get('/contact', 'HomeController@contact')->name('contact');
Route::group(['prefix' => 'services'], function(){
    Route::get('/web-development', 'ServicesController@web_dev')->name('service.web');
    Route::get('/app-development', 'ServicesController@app_dev')->name('service.app');
    Route::get('/domain-registration', 'ServicesController@domain')->name('service.domain');
    Route::get('/web-hosting', 'ServicesController@web_host')->name('service.hosting');
});
// Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
// Route::get('list-accounts',function(){
//     $list_accounts = CpanelWhm::listpkgs();

//     return $list_accounts;
// });

Route::get('auth/facebook', 'Auth\LoginController@redirectToProvider');
Route::get('auth/facebook/callback', 'Auth\LoginController@handleProviderCallback');
Route::post('/login', 'Auth\LoginController@login')->name('login.submit');
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
// ROUTE UNTUK HALAMAN USER PANEL
Route::group(['prefix' => 'user'], function(){
    Route::get('/dashboard', 'User\DashboardController@index')->name('user.dashboard');
    Route::get('/logout', 'Auth\LoginController@userLogout')->name('user.logout');
});


// ROUTE UNTUK HALAMAN ADMIN PANEL
Route::group(['prefix' => 'admin'], function(){
    Route::get('/login', 'AuthAdmin\LoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'AuthAdmin\LoginController@login')->name('admin.login.submit');
    Route::get('/', 'AdminController@index')->name('admin.home');
    Route::get('/logout', 'AuthAdmin\LoginController@logout')->name('admin.logout');
    Route::get('/clients', 'Admin\ClientController@index')->name('admin.clients');
});



